import threading
from MCP25625_api import MCP25625_api, Message
from PIL import Image
import datetime
import io
import time

class can_sample(object):

    def __init__(self):
        # Setup Listener thread
        self.listening = threading.Event()
        self.listening.set()    # clear to stop listening
        #self.listenThread = threading.Thread(target=self.ListenThread)
        #self.listenThread.daemon = True
        
        # Setup CAN API
        self.can = MCP25625_api()
        self.can.Initialize()
        
        self.can.SetNormalMode() # Uncomment for normal mode.
        #self.can.SetLoopbackMode() # Uncomment for loopback (debug) mode.
            
    def Start(self):
        # Start Listener thread
        #self.listenThread.start()
        print("Started running sender at: " + str(datetime.datetime.now()))

        with open("pic.jpg", "rb") as file:
            allbytes = bytearray(file.read())

        arbitration_id = 0b10010010010010010011111111111 # Extended Mode, 29bit#
        arbitration_id = 0b1

        firstPacket = True
        sentNum = 0
        packet = []
        alldata = []
        count = 1
        for byte in allbytes:
            alldata.append(int(byte))
            packet.append(int(byte))
            if len(packet) == 8:
                if firstPacket:
                    print(packet)
                    firstPacket = False

                data = packet
                packet = []
                msg = Message(arbitration_id, data) #, extended_id=False)

                count += 1

                try:
                    self.can.Send(msg, timeoutMilliseconds=1000)
                    sentNum += 1
                except TimeoutError as e:
                    print("Timeout Error: " + str(e))

        msg = Message(arbitration_id, packet)
        print(packet)
        self.can.Send(msg, timeoutMilliseconds=1000)
        sentNum += 1
        print("Sent: " + str(sentNum) + " packets")
    
        print("There are: " + str(count) + " packets, with " + str(count*8) + " pieces of data")
        print("Ended sender at: " + str(datetime.datetime.now()))

        alldata = bytearray(alldata)
        image = Image.open(io.BytesIO(alldata))
        image.save("copy.jpg")

        self.can.Send(Message(0b1, [0,0,0,0,0,0,0,0]), timeoutMilliseconds=1000)

    def ListenThread(self):
        print("RECV: Listening for CAN messages...")
        while (self.listening.is_set()):
            try:
                recvMsg = self.can.Recv(timeoutMilliseconds=None)
                print("\nRECV: {0}".format(recvMsg))
            except TimeoutError as e:
                print("\nRECV: Timeout receiving message. <{0}>".format(e))
        
        print("RECV: Listener stopped.")

if __name__ == "__main__":
    print("Running PicSend.py through shell/idle.bat/ect.")
    sample = can_sample()
    sample.Start()
def PicSendMain():
    print("Running PicSend.py through a function call")
    sample = can_sample()
    sample.Start()
