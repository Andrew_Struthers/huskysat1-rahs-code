import os
import io
import PicSend
import datetime
from PIL import Image
from time import sleep
from picamera import PiCamera

def takePic():
    global logfile

    camera = PiCamera()

    camera.resolution = (640, 480)
    camera.vflip = True
    camera.capture('pic.jpg')

    compress('pic.jpg')

def compress(file):
    global logfile
    
    filepath = os.path.join(os.getcwd(), file)
    oldsize = os.stat(filepath).st_size

    picture = Image.open(filepath)
    dim = picture.size

    picture.save(file)

    newsize = os.stat(os.path.join(os.getcwd(), file)).st_size
    percent = (oldsize-newsize)/float(oldsize)*100

    print("File compressed from " + str(oldsize) + " to " + str(newsize) + " or " + str(percent) + "%")
    ##logfile.write("File compressed from " + str(oldsize) + " to " + str(newsize) + " or " + str(percent) + "%")
def checkDir():
    global logfile
    
    if not os.path.isdir("photoArchive"):
        os.makedirs("photoArchive")

def moveAndRenamePic():
    global logfile
    
    curTime = str(datetime.datetime.now()).split(" ")
    name = curTime[0] + "_" + curTime[1].split(".")[0]
    os.popen("cp pic.jpg photoArchive/"+str(name)+".jpg")

def lookAtPic():
    global logfile
    
    with open("pic.jpg", "rb") as file:
        allbytes = file.read()
    packet = []
    packetcount = 0
    data = []
    datacount = 0
    for byte in allbytes:
        datacount += 1
        data.append(byte)
        packet.append(byte)
        if len(packet) == 8:
            packet = []
            packetcount += 1
    packetcount = float(packetcount)
    packetcount += len(packet)/8

    print("There are: " + str(packetcount) + " packets (8-byte arrays) in pic.jpg")
    print("The last packet only has " + str(len(packet)) + " bytes in it")
    print("There are: " + str(datacount) + " pieces of data in pic.jpg")
    print(str(datacount/8) + " should equal " + str(packetcount) + " --> " + str(float(datacount/8) == float(packetcount)))
    print(str(packetcount*8) + " should equal " + str(datacount) + " --> " + str(float(packetcount*8) == float(datacount)))
    print("Because average send time is 0.1 seconds/packet right now, expected send time will be: " + str((packetcount*0.1)/60) + " minutes. Sorry.")

#########################################################################################
os.chdir(os.getcwd())

if not os.path.exists("log.txt"):
    logopen = open("log.txt", "w+")
    logopen.close()

logfile = open("log.txt", "w+")

takePic()
#checkDir()
#moveAndRenamePic()

sleep(1) #pause between taking/modifying/handling/etc. picture and running the send code    

lookAtPic()
#os.popen("python3 new_send.py")

import new_send
new_send.new_send_main()

logfile.close()

#Carson is gonna make the logfile now
